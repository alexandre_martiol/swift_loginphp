//
//  WelcomeViewController.swift
//  LoginPHP
//
//  Created by Alexandre Martinez Olmos on 10/3/15.
//  Copyright (c) 2015 AMO. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {
    @IBOutlet weak var mailUserText: UILabel!
    var userMail: String!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.mailUserText.text = userMail
    }

    @IBAction func logoutAction(sender: AnyObject) {
        let alertController = UIAlertController(title: "Log out", message: "Do you want to log out?", preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
            println(action)
        }
        alertController.addAction(cancelAction)
        
        let logoutAction = UIAlertAction(title: "Log Out", style: .Destructive) { (action) in
            println(action)
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc: UIViewController = storyboard.instantiateViewControllerWithIdentifier("ViewController") as! ViewController
            self.presentViewController(vc, animated: true, completion: nil)
        }
        alertController.addAction(logoutAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
