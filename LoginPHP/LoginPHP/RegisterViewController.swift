//
//  RegisterViewController.swift
//  LoginPHP
//
//  Created by Alexandre Martinez Olmos on 11/3/15.
//  Copyright (c) 2015 AMO. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {
    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var emailText: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func registerAction(sender: AnyObject) {
        //primero compruebo que se han introducido los datos
        if (usernameText.text == "" || passwordText.text == "" || emailText.text == ""){
            //muestra un AlertView de error al usuario
            let alertController = UIAlertController(title: "Empty Fields", message: "There are empty fields...", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
            
        else{
            var username = usernameText.text
            var password = passwordText.text
            var email = emailText.text
            
            //Check if email is correct
            if(isValidEmail(email)) {
                var url: NSURL = NSURL(string:"http://localhost/WorkSpace/LoginSwift/Singin.php")!
                var request: NSMutableURLRequest = NSMutableURLRequest(URL:url)
                
                //indico que el método para pasar datos es POST
                request.HTTPMethod = "POST"
                
                //paso datos: pares ‘nombre_variable=valor’
                //los nombres que pongo son los mismos que recoge el PHP de la variable $_POST
                var bodyData = "nombre=\(username)&pass=\(password)&email=\(email)"
                request.HTTPBody = bodyData.dataUsingEncoding(NSUTF8StringEncoding)
                
                NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {(response, data, error) in
                    let res = response as? NSHTTPURLResponse
                    if (res != nil){
                        var responseData:NSString = NSString(data:data!, encoding:NSUTF8StringEncoding)!
                        //El código de estado de http si todo ha ido bien es el 200
                        
                        if (res!.statusCode == 200){
                            let jsonData: NSDictionary = NSJSONSerialization.JSONObjectWithData(data!, options:NSJSONReadingOptions.MutableContainers , error: nil) as! NSDictionary
                            
                            //cojo el dato ‘success’ que devuelve PHP
                            let success:NSInteger = jsonData.valueForKey("success")as! NSInteger
                            
                            if (success == 1){
                                println("USER \(username) HAS REGISTERED CORRECTLY!")
                                //ir a bienvenida
                                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc: WelcomeViewController = storyboard.instantiateViewControllerWithIdentifier("WelcomeViewController") as! WelcomeViewController
                                
                                vc.userMail = "User: \(email)"
                                
                                self.presentViewController(vc, animated: true, completion: nil)
                            }
                                
                            else{
                                //Si ‘success’ no es 1 recojo el mensaje de error que devuelve PHP
                                if (jsonData["mensaje_error"] != nil){
                                    var error: AnyObject! = jsonData["mensaje_error"]
                                    //muestra un AlertView con el mensaje de error
                                    let alertController = UIAlertController(title: "Registration error", message: "\(error)", preferredStyle: UIAlertControllerStyle.Alert)
                                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
                                    
                                    self.presentViewController(alertController, animated: true, completion: nil)
                                }
                            }
                        }
                            
                        else {
                            println(response)
                        }
                    }
                }
            
            }
            
            else {
                let alertController = UIAlertController(title: "Incorrect Email", message: "You must to write a valid email", preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
                
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        }
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let range = testStr.rangeOfString(emailRegEx, options:.RegularExpressionSearch)
        let result = range != nil ? true : false
        return result
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
