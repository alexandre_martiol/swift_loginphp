//
//  ViewController.swift
//  LoginPHP
//
//  Created by AlexM on 10/03/15.
//  Copyright (c) 2015 AMO. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var passwordText: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func loginAction(sender: AnyObject) {
        //primero compruebo que se han introducido los datos
        if (usernameText.text == "" || passwordText.text == ""){
            //muestra un AlertView de error al usuario
            let alertController = UIAlertController(title: "Empty Fields", message: "There are empty fields...", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
            
        else{
            var username = usernameText.text
            var password = passwordText.text
            var url: NSURL = NSURL(string:"http://localhost/WorkSpace/LoginSwift/Login.php")!
            var request: NSMutableURLRequest = NSMutableURLRequest(URL:url)
            
            //indico que el método para pasar datos es POST
            request.HTTPMethod = "POST"
            
            //paso datos: pares ‘nombre_variable=valor’
            //los nombres que pongo son los mismos que recoge el PHP de la variable $_POST
            var bodyData = "nombre=\(username)&pass=\(password)"
            request.HTTPBody = bodyData.dataUsingEncoding(NSUTF8StringEncoding)
            
            NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {(response, data, error) in
                    let res = response as? NSHTTPURLResponse
                    if (res != nil){
                        var responseData:NSString = NSString(data:data!, encoding:NSUTF8StringEncoding)!
                        //El código de estado de http si todo ha ido bien es el 200
                        
                        if (res!.statusCode == 200){
                            let jsonData: NSDictionary = NSJSONSerialization.JSONObjectWithData(data!, options:NSJSONReadingOptions.MutableContainers , error: nil) as! NSDictionary
                            
                            //cojo el dato ‘success’ que devuelve PHP
                            let success:NSInteger = jsonData.valueForKey("success")as! NSInteger
                            
                            if (success == 1){
                                println("USER \(username) HAS LOGGED IN CORRECTLY!")
                                //ir a bienvenida
                                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc: WelcomeViewController = storyboard.instantiateViewControllerWithIdentifier("WelcomeViewController") as! WelcomeViewController
                                
                                var userMail: AnyObject! = jsonData.valueForKey("email")
                                vc.userMail = "User: \(userMail)"
                            
                                self.presentViewController(vc, animated: true, completion: nil)
                            }
                                
                            else{
                                //Si ‘success’ no es 1 recojo el mensaje de error que devuelve PHP
                                if (jsonData["mensaje_error"] != nil){
                                    var error: AnyObject! = jsonData["mensaje_error"]
                                    //muestra un AlertView con el mensaje de error
                                    let alertController = UIAlertController(title: "Login error", message: "\(error)", preferredStyle: UIAlertControllerStyle.Alert)
                                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
                                    
                                    self.presentViewController(alertController, animated: true, completion: nil)
                                }
                            }
                        }
                            
                        else {
                            println(response)
                        }
                    }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

